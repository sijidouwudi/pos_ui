/* jshint esversion: 6 */
import Vue from 'vue';
import VueRouter from 'vue-router';
import iView from 'iview';
import 'iview/dist/styles/iview.css';
import {
    getToken
} from '@/libs/util';


Vue.use(iView);
Vue.use(VueRouter);
const router = new VueRouter({
    mode: 'hash',
    routes: [
        {
            path: '/',
            meta: {
                auth: true,
            },
            component: () => import('@/views/Main.vue'),
            children: [
                {
                    path: '/welcome',
                    name: 'Welcome',
                    meta: {
                        parentText: '欢迎页',
                        text: '欢迎页'
                    },
                    component: () => import('@/views/Welcome.vue')
                },
            ]
        },
        {
            path: '/login',
            name: 'Login',
            component: () => import('@/views/Login.vue')
        }
    ]
});

router.beforeEach((to, from, next) => {
    var token = getToken()
    console.log(token)
    if (to.matched.some(res => res.meta.auth)) { // 判断是否需要登录权限
        if (token) { // 判断是否登录
            next()
        } else { // 没登录则跳转到登录界面
            next({
                path: '/login',
            });
        }
    } else {
        next();
    }
});

export default router;