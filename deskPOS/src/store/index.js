/* jshint esversion: 6 */
import Vue from 'vue';
import Vuex from 'vuex';
// import {goodsCategoryList,UserTypeList,userLevelList,streetList, userDetails} from "../axios/api";
import {getItem} from "../libs/tools";

Vue.use(Vuex);

export default new Vuex.Store({
  // 状态
  state: {
    //总公司商品分类
    categoryArr:[],
    // 门店商品分类
    storeCategoryArr:[],
    // 用户分类
    userTypeArr: [],
    //用户等级
    userLevelArr: [],
    //街道数组
    streetArr: [],
    //用户ID
    userId:'',
    //用户信息
    userInfo: {}
  },
  // 计算属性
  getters: {

  },
  // mutations
  mutations: {
    //保存用户ID
    saveUserId(state, value){
      state.userId = value
    },
    //保存用户信息
    saveUserInfo(state, value){
      state.userInfo = value
    },
    //保存总公司商品分类数据
    saveCategoryArr(state,value){
      state.categoryArr = value
    },
    // 保存门店商品分类数据
    saveStoreCategoryArr(state,value){
      state.storeCategoryArr = value
    },
    //保存用户分类数据
    saveUserType(state, value){
      state.userTypeArr = value
    },
    //保存用户等级
    saveUserLevel(state, value){
      state.userLevelArr = value
    },
    // 保存街道信息
    saveStreetList(state, value){
      state.streetArr = value
    }
  },
  // actions
  actions: {
    // 查询商品分类
    // queryGoodsTypeList({state, commit}) {
    //   var params = {
    //     current: 1,
    //     size: 100000,
    //     params: {
    //       orgId: -1
    //     }
    //   }
    //   goodsCategoryList(params).then(res => {
    //     if (res.data.flag === 1) {
    //       const data = res.data.data.records
    //       commit('saveCategoryArr', data)
    //     }
    //   })
    // },
   
  }
});