/* jshint esversion: 6 */
import Vue from 'vue';
import App from './App.vue';
import router from '@/router';
import store from '@/store';
import iviewArea from 'iview-area';
import '@/assets/style/style.less';
import VueParticles from 'vue-particles';
import BaiduMap from 'vue-baidu-map';
import {objectsIntersection} from "./libs/tools";


Vue.use(VueParticles);
Vue.use(BaiduMap,{
  ak:'GcLGaQ2N14PY9H0RmMT70lAjApp6qx8a' //官方密钥
})
Vue.use(iviewArea);
Vue.config.productionTip = false;

// 注册一个全局过滤方法，用于过滤对象中不需要的参数
Vue.prototype.objectsIntersection = objectsIntersection

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');