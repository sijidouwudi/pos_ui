/* jshint esversion: 6 */

// 本地保存数据与取数据操作
export function setItem(key, value) {
    localStorage.setItem(key, value);
}

export function getItem(key) {
    var value = localStorage.getItem(key);
    return value;
}

export function clearItem(key) {
    if (key) {
        localStorage.clear(key);
    } else {
        localStorage.clear();
    }
}

// 生成随机数
export function renderNum(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

/**
 * @description 绑定事件 on(element, event, handler)
 */
export const on = (function () {
    if (document.addEventListener) {
        return function (element, event, handler) {
            if (element && event && handler) {
                element.addEventListener(event, handler, false)
            }
        }
    } else {
        return function (element, event, handler) {
            if (element && event && handler) {
                element.attachEvent('on' + event, handler)
            }
        }
    }
})()

/**
 * @description 解绑事件 off(element, event, handler)
 */
export const off = (function () {
    if (document.removeEventListener) {
        return function (element, event, handler) {
            if (element && event) {
                element.removeEventListener(event, handler, false)
            }
        }
    } else {
        return function (element, event, handler) {
            if (element && event) {
                element.detachEvent('on' + event, handler)
            }
        }
    }
})()

/**
 * @param {String|Number} value 要验证的字符串或数值
 * @param {*} validList 用来验证的列表
 */
export function oneOf(value, validList) {
    for (let i = 0; i < validList.length; i++) {
        if (value === validList[i]) {
            return true
        }
    }
    return false
}

/**
 * 判断某元素是否已经存在于某对象数组
 * @param value
 * @param array
 * @param name
 */
export function objInObjectArray(value, array, name) {
    var bool = false
    if(array.length===0){
        return bool
    }
    array.forEach((item)=> {
        bool = item[name] === value;
    })
    return bool
}

/**
 * 两个对象求交集
 * @param temp
 * @param params
 */
export function objectsIntersection(temp,params) {
    var obj = params
    var p = Object.keys(params)
    p.forEach((item)=>{
        if(!temp.hasOwnProperty(item)){
            delete obj[item]
        }
    })
    return obj
}