/* jshint esversion: 6 */
import Cookies from 'js-cookie';
import Echarts from 'echarts';
import Monent from 'moment';
import {
  on
} from './tools';
const cookieExpires = 1;

export const TOKEN_KEY = 'token';

export const setToken = token => {
  Cookies.set(TOKEN_KEY, token, {
    expires: cookieExpires
  });
};

export const getToken = () => {
  const token = Cookies.get(TOKEN_KEY);
  if (token) return token;
  else return false;
};


// echarts图表方法
export const echats = (_options, id) => {
  var myChart = Echarts.init(document.getElementById(id));

  // 指定图表的配置项和数据
  var options = {};
  var option = {
    color: [
      '#8c0776',
      "#2d8cf0",
      "#19be6b",
      "#f5ae4a",
      "#9189d5",
      "#56cae2",
      "#cbb0e3"
    ],
    grid: {
      left: '2%',
      right: '2%',
      bottom: '3%',
      top: '5%',
      containLabel: true
    }
  };
  Object.assign(options, option, _options);

  // 使用刚指定的配置项和数据显示图表。
  myChart.setOption(options);
  myChart.resize();
  on(window, 'resize', function () {
    myChart.resize();
  });
};

/**
 * 日期格式化
 * @param date
 * @returns {string}
 */
export function formatDate(date) {
  var newDate = Monent(date).format('YYYY-MM-DD HH:mm:ss')
  return newDate
}

/**
 * JSON转FormData
 * @param data
 * @returns {string|string}
 */
export function objectToFormData(data) {
  let ret = '';
  for (let it in data) {
    ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&';
  }
  return ret;
}