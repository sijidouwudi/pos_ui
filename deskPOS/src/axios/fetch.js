/* jshint esversion: 6 */
import Vue from 'vue';
import axios from 'axios';
import router from '@/router';
import {
    getToken,
    objectToFormData
} from '@/libs/util';
import {url} from "./url";
import iView from 'iview';
import '@/assets/style/style.less';

Vue.use(iView)
export function fetch(options) {
    return new Promise((resolve, reject) => {
        let token = getToken();
        const instance = axios.create({
            // instance 创建一个axios实例，可以自定义配置，可在axios文档中查看详情
            // 所有的请求都会带上这些配置，比如全局都要用的身份信息等。
            baseURL: url,
            headers: {
                'Content-Type': options.contentType || 'application/json',
                'X-Access-Token': token
            },
            timeout: 9000 // 超时
        });

        // 请求拦截器
        instance.interceptors.request.use(
            config => {
                // 在发送请求前
                if (config.method === 'post') {
                    if (options.contentType) {
                        config.data = objectToFormData(config.data);
                    } else {
                        config.data = JSON.stringify(config.data);
                    }
                }
                return config;
            },
            err => {
                // 对请求错误做些什么
                return Promise.reject(err);
            }
        );

        // 响应拦截器
        instance.interceptors.response.use(
            res => {
                if (res.data.flag === 2) {
                    iView.Message.error(res.data.message)
                } else if (res.data.flag === 3) {
                    iView.Message.error('登录已经失效,即将跳转登录页...')
                    setTimeout(() => {
                        router.push({path: '/login'})
                    }, 3000)
                }
                // 在响应前
                return res;
            },
            err => {
                // 对响应错误做些什么
                return Promise.reject(err);
            }
        );

        instance(options)
            .then(res => {
                // then请求成功之后进行的操作
                resolve(res); // 把请求到的数据发送引用请求的地方
            })
            .catch(err => {
                reject(err);
            });
    });
}