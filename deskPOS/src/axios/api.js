import {fetch} from "./fetch";
import {url} from "./url";

/**
 * 这里存放数据接口
 */

/**
 * 登录接口
 * @param params
 * @returns {*|Promise<any>|Window.Promise}
 */
export function login(params) {
    return fetch({
        url: 'sys/tlogin',
        method: 'POST',
        data: params
    })
}

//post
export function postAction(url,parameter) {
    return fetch({
      url: url,
      method:'post' ,
      data: parameter
    })
  }
  
  //post method= {post | put}
  export function httpAction(url,parameter,method) {
    return fetch({
      url: url,
      method:method ,
      data: parameter
    })
  }
  
  //put
  export function putAction(url,parameter) {
    return fetch({
      url: url,
      method:'put',
      data: parameter
    })
  }
  
  //get
  export function getAction(url,parameter) {
    return fetch({
      url: url,
      method: 'get',
      params: parameter
    })
  }
  
  //deleteAction
  export function deleteAction(url,parameter) {
    return fetch({
      url: url,
      method: 'delete',
      params: parameter
    })
  }

